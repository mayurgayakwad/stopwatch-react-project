import "./App.css";
import React, { Component } from "react";
import Timer from "./Component/timer";

class App extends Component {
  constructor() {
    super();
    this.state = {
      change: 1,
    };
  }
  change = () => {
    this.setState({ change: (this.state.change = 2) });
  };
  close = () => {
    this.setState({ change: (this.state.change = 1) });
  };
  render() {
    return (
      <>
        <center>
          <h1>🚀 Timer 🚀</h1>
          {this.state.change == 1 ? (
            <button onClick={this.change}>Show Stop Watch</button>
          ) : this.state.change == 2 ? (
            <>
              <Timer />{" "}
              <button onClick={this.close} className="CloseBtn">
                Close
              </button>
            </>
          ) : (
            ""
          )}
        </center>
      </>
    );
  }
}

export default App;
