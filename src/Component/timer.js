import React, { Component } from "react";
import "../Component/timerstyle.css";

class Timer extends Component {
  constructor() {
    super();
    this.state = {
      button: 1,
      hours: 0,
      min: 0,
      sec: 0,
      csec: 0,
    };
    this.interval = null;
  }

  startTimer = () => {
    this.setState({ csec: this.state.csec + 1 });
    if (this.state.csec === 100) {
      this.setState({ sec: this.state.sec + 1 });
      this.setState({ csec: (this.state.csec = 0) });
    }
    if (this.state.sec === 60) {
      this.setState({ min: this.state.min + 1 });
      this.setState({ sec: (this.state.sec = 0) });
    }
    if (this.state.min === 60) {
      this.setState({ sec: this.state.hours + 1 });
      this.setState({ min: (this.state.min = 0) });
    }
  };

  startIntveral = () => {
    this.interval = setInterval(this.startTimer, 10);
  };
  start = () => {
    this.setState({ button: (this.state.button = 2) });
    this.startIntveral();
  };

  stop = () => {
    this.setState({ button: (this.state.button = 3) });
    clearInterval(this.interval);
  };
  Resume = () => {
    this.setState({ button: this.state.button - 1 });
    this.startIntveral();
  };
  Reset = () => {
    this.setState({ button: (this.state.button = 1) });
    this.setState({ csec: (this.state.csec = 0) });
    this.setState({ sec: (this.state.sec = 0) });
    this.setState({ min: (this.state.min = 0) });
    this.setState({ hours: (this.state.hours = 0) });
  };
  componentWillUnmount() {
    this.Reset();
    clearInterval(this.interval);
  }
  render() {
    return (
      <center>
        <div className="stopwatchContainer">
          <h1 className="Stopwastch">StopWatch</h1>
          <h2>
            <span>
              {this.state.hours === 0
                ? "0" + this.state.hours
                : this.state.hours}{" "}
              :
            </span>
            <span>
              {" "}
              {this.state.min === 0
                ? "0" + this.state.min
                : this.state.min}{" "}
            </span>
            :
            <span>
              {" "}
              {this.state.sec < 10 ? "0" + this.state.sec : this.state.sec}{" "}
            </span>
            :
            <span>
              {" "}
              {this.state.csec < 10
                ? "0" + this.state.csec + ""
                : this.state.csec === 100
                ? "00"
                : this.state.csec}{" "}
            </span>
          </h2>
          {this.state.button === 1 ? (
            <button onClick={this.start}>Start</button>
          ) : this.state.button === 2 ? (
            <button onClick={this.stop}>Stop</button>
          ) : (
            <>
              <button onClick={this.Resume}>Resume</button>
              <button onClick={this.Reset}>Reset</button>
            </>
          )}
        </div>
      </center>
    );
  }
}

export default Timer;
